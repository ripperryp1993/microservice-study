from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO

def update_account(ch, method, properties, body):
    content = json.loads(body)
    first_name = content['first_name']
    last_name = content['lasto_name']
    email = content['email']
    is_active = content['is_active']
    updated_string = content['updated']
    if is_active:
        AccountVO.objects.update_or_create(
            first_name=first_name,
            last_name=last_name,
            email=email,
            updated=updated_string
        )
    else:
        AccountVO.objects.delete(email=email)
    print("  Received %r" % body)
    
while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.exchange_declare(exchange='account_info', exchange_type='fanout')
        channel.queue_declare(queue='create_or_update')
        channel.queue_bind(exchange='account_info', queue='create_or_update')
        channel.basic_consume(
            queue='create_or_update',
            on_message_callback=update_account,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)